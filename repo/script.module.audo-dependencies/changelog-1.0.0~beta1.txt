0.0.5~beta
- temp patch to work on armv7 linux (I think it will break OE)

0.0.4
- temp fix for SSL issues with kodi
- pyopenssl 0.11
- added openssl 1.0.1h libs

0.0.3
- unrar - 5.2.2

0.0.2
- removed bytecode for multiple interpreter compatibility
- added binaries for armv7l devices
- removed openelec requirement should work on all linux xbmc installs

0.0.1
- initial Release
- Cheetah - 2.4.4
- yenc - 0.4.0
- configobj - 4.7.2
- openssl - 1.0.1e
- unrar - 5.0.1
- par2 - 0.4
